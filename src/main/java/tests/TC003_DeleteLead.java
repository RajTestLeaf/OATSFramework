package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testCaseDescription ="Delete a lead";
		category = "Sanity";
		author= "Raj";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String cname,String fname, String Lname)   {
		new MyHomePage()
		.clickLeads()
		.selectLead()
		.clickEditButton()
		.typeCompanyName(cname)
		.typeFirstName(fname)
		.typeLastName(Lname)
		.clickCreateLead();	
	}
}
