package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {
public EditLeadPage() {
	PageFactory.initElements(driver, this);
}
@CacheLookup
@FindBy(linkText="Edit")
WebElement eleClickEdit;
public EditLeadPage clickEditButton() {
	//WebElement eleClickEdit = locateElement("linktext", "Edit");
	click(eleClickEdit);
	return this;
}
@CacheLookup
@FindAll({@FindBy(id="updateLeadForm_companyName"),@FindBy(name="companyName")})
WebElement eleCompanyName;
public EditLeadPage typeCompanyName(String data) {
	//WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
	type(eleCompanyName, data);
	return this;
}
@CacheLookup
@FindAll({@FindBy(id="updateLeadForm_firstName"),@FindBy(name="firstName")})
WebElement eleFirstName;
public EditLeadPage typeFirstName	(String data) {
	//WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
	type(eleFirstName, data);
	return this;
}
@CacheLookup
@FindAll({@FindBy(name="lastName"),@FindBy(id="updateLeadForm_lastName")})
WebElement eleLastName;
public EditLeadPage typeLastName	(String data) {
	//WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
	type(eleLastName, data);
	return this;
}
@CacheLookup
@FindAll({@FindBy(className="smallSubmit"),@FindBy(name="submitButton")})
WebElement eleClickUpdate;
public EditLeadPage clickUpdate() {
	//WebElement eleClickUpdate = locateElement("xpath", "//input[@class='smallSubmit']");
	click(eleClickUpdate);
	return this;
}


public EditLeadPage clickCreateLead() {
	WebElement eleCreateLead= locateElement("class", "smallSubmit");
	click(eleCreateLead);
	return this; 
}
}
