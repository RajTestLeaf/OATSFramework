package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
public MyLeadsPage() {
	PageFactory.initElements(driver, this);
}
	
	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	@CacheLookup
	@FindAll({@FindBy(xpath="(//a[@href='/crmsfa/control/viewLead?partyId=10115'])[1]"),@FindBy(linkText="10059")})
	WebElement eleSelectLead;
	public EditLeadPage selectLead() {
		//WebElement eleSelectLead = locateElement("linktext", "10059");
		click(eleSelectLead);
		return new EditLeadPage();
	}
	
}









