package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DeleteLeadPage extends ProjectMethods{
public DeleteLeadPage() {
	PageFactory.initElements(driver, this);
}
@CacheLookup
@FindAll({@FindBy(linkText="Delete"),@FindBy(className="subMenuButtonDangerous")})
WebElement eleClickDelete;
public DeleteLeadPage clickDelete() {
	click(eleClickDelete);
	return this;
}
}
